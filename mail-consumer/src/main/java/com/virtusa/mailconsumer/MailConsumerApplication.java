package com.virtusa.mailconsumer;

import java.time.Duration;
import java.util.Arrays;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.virtusa.mailconsumer.services.ConsumerService;


@SpringBootApplication
public class MailConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailConsumerApplication.class, args);
		KafkaConsumer<Integer, String> consumer=ConsumerService.getConsumer();
		consumer.subscribe(Arrays.asList("mail_topic"));
		while(true){
	         ConsumerRecords<Integer, String> records = consumer.poll(Duration.ofSeconds(1L));
	         for (ConsumerRecord<Integer, String> record : records)
	         System.out.println(record.value());
	    }
	}

}
