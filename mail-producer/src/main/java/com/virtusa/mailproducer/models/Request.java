package com.virtusa.mailproducer.models;

import java.util.List;

public class Request {
	private String quoteRequestNumber="";
	private List<String> data;
	public String getQuoteRequestNumber() {
		return quoteRequestNumber;
	}
	public void setQuoteRequestNumber(String quoteRequestNumber) {
		this.quoteRequestNumber = quoteRequestNumber;
	}
	public List<String> getData() {
		return data;
	}
	public void setData(List<String> data) {
		this.data = data;
	}
	
}
