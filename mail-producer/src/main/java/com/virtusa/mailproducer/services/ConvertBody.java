package com.virtusa.mailproducer.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;
import com.virtusa.mailproducer.models.Request;

public class ConvertBody {
	
	public static String getJson(Message message) {
		String json ="";
		try {
	        String messageContent=MailBody.getHTML(message);
	        Request req=new Request();
	        Document doc = Jsoup.parse(messageContent);
	        Element table = doc.select("table").get(0);
	        Elements rows = table.select("tr");
	        int n=rows.get(0).select("td").size();
	        
	        Elements topdata=doc.select("span");
	        String dp=topdata.get(1).text();
	        String tms=topdata.get(2).text().substring(7);
	        String cmd=topdata.get(3).text().substring(10);
	        String opc="";
	        String pp="";
	        String dpc="";
	        
	        List<String> l=new ArrayList<String>();
	        List<String> headings=new ArrayList<String>();
	        if(n==2) {
	        	for(int i=0;i<rows.size();i++) {
	        		Element row = rows.get(i);
		            Elements cols = row.select("td");
		            l.add(cols.get(0).text());
		            l.add(cols.get(1).text());   
	        	}
	        	l.add("DeliveryPoint"); l.add(dp);
	            l.add("Terms"); l.add(tms);
	            l.add("Commodity"); l.add(cmd);
	            l.add("OriginPostalCode"); l.add(opc);
	            l.add("PickupPoint"); l.add(pp);
	            l.add("DestionationPostalCode"); l.add(dpc);
	        }
	        else {
	        	for(int i=0;i<n;i++) {
		        	headings.add(rows.get(0).select("td").get(i).text()); }
	        	for (int r = 1; r < rows.size(); r++) { 
		            Element row = rows.get(r);
		            Elements cols = row.select("td");
		            for(int i=0;i<n;i++) {
		            	l.add(headings.get(i));
		            	l.add(cols.get(i).text());
		            }
		            l.add("DeliveryPoint"); l.add(dp);
		            l.add("Terms"); l.add(tms);
		            l.add("Commodity"); l.add(cmd);
		            l.add("OriginPostalCode"); l.add(opc);
		            l.add("PickupPoint"); l.add(pp);
		            l.add("DestionationPostalCode"); l.add(dpc);
		        }
	        }
	        req.setData(l);
	        Gson gson = new Gson();
            json =gson.toJson(req);
            
        }catch (IndexOutOfBoundsException e){return "Invalid Body!"; }
		 catch (MessagingException e) {e.printStackTrace();}  
		 catch (IOException e) {e.printStackTrace();}
        
		return json;
	}
}
