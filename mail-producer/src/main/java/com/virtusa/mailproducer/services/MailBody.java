package com.virtusa.mailproducer.services;

import java.io.IOException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;

public class MailBody {
	public static String getHTML(Message message) throws MessagingException, IOException {
		 String messageContent="";
		 String contentType = message.getContentType();
	     if (contentType.contains("multipart")) {
	            Multipart multiPart = (Multipart) message.getContent();
	            for (int i=0;i<multiPart.getCount();i++) {
	                MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(i);
	                messageContent = part.getContent().toString();
	            }
	      }
	      else if (contentType.contains("text/plain")|| contentType.contains("text/html")) {
	            Object content = message.getContent();
	            if (content != null) {
	                messageContent = content.toString();
	            }
	        }
	     return messageContent;
		
	}

}
