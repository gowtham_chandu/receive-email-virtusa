package com.virtusa.mailproducer.services;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Flags.Flag;
import javax.mail.search.FlagTerm;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class Reciever {
	
	public static void receiveEmail(String pop3Host, String storeType, String user, String password) throws InterruptedException {  
		  try {  
			  Properties properties = new Properties();  
		      properties.put("mail.store.protocol", "imaps");  
		      Session emailSession = Session.getDefaultInstance(properties,
		      new Authenticator() {
		          protected PasswordAuthentication getPasswordAuthentication() {
		                        return new PasswordAuthentication(user,password);
		                    }
		      }); 
		      Producer<Integer, String> producer = ProducerService.getProducer();
		      int cnt=1;
		      Store emailStore = emailSession.getStore("imaps");
		      emailStore.connect("imap.gmail.com",user, password);
		      for(int t=0;t<1000;t++) {
			      Folder emailFolder = emailStore.getFolder("INBOX");  
			      emailFolder.open(Folder.READ_WRITE);  
			      Message[] messages = emailFolder.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));
			      for (int i = 0; i < messages.length; i++) {  
					    messages[i].setFlag(Flag.SEEN, true);
					    Message message = messages[i];
				        String json = ConvertBody.getJson(message);
				        producer.send(new ProducerRecord<Integer, String>("mail_topic",cnt++,json));
				        System.out.println(json);
			      }
			      emailFolder.close(false); 
			      Thread.sleep(10000);
		      }
		      emailStore.close(); 
		      
		  } catch (MessagingException e) {e.printStackTrace();}  
		 }  

}
